package com.gemframework.modules.prekit.quartz.service;

import com.gemframework.GemAdminApplication;
import com.gemframework.modules.prekit.quartz.entity.JobsVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GemAdminApplication.class)
@Slf4j
class QuartzManagerTest {

    @Autowired
    QuartzManager quartzManager;

    @Test
    void createJob() throws InterruptedException {
        JobsVo jobsVo = new JobsVo();
        jobsVo.setExecuteClassName("JobDemo");
        jobsVo.setJobName("测试任务1");
        jobsVo.setJobGroup("测试");
        jobsVo.setTriggerName("触发器1");
        jobsVo.setTriggerGroup("触发");
        jobsVo.setTriggerCronExpression("0/5 * * * * ?");
        jobsVo.setJobType(2);
        quartzManager.startJob(jobsVo);
        // 让主线程睡眠60秒
        Thread.currentThread().sleep(60000);
    }
}