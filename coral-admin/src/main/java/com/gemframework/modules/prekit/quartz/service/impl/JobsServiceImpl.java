/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gemframework.common.config.GemSystemProperties;
import com.gemframework.common.config.quartz.GemQuartzProperties;
import com.gemframework.common.utils.GemBeanUtils;
import com.gemframework.modules.prekit.quartz.entity.Jobs;
import com.gemframework.modules.prekit.quartz.entity.JobsVo;
import com.gemframework.modules.prekit.quartz.entity.enums.JobInit;
import com.gemframework.modules.prekit.quartz.entity.enums.JobStatus;
import com.gemframework.modules.prekit.quartz.mapper.JobsMapper;
import com.gemframework.modules.prekit.quartz.service.JobsService;
import com.gemframework.modules.prekit.quartz.service.QuartzManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Title: JobsServiceImpl
 * @Date: 2020-06-20 19:23:45
 * @Version: v1.0
 * @Description: 服务实现类
 * @Author: gem
 * @Email: gemframe@163.com
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
@Service
@Transactional
public class JobsServiceImpl extends ServiceImpl<JobsMapper, Jobs> implements JobsService {

    @Autowired
    QuartzManager quartzManager;

    @Autowired
    JobsMapper jobsMapper;

    @Autowired
    GemSystemProperties gemSystemProperties;

    @Autowired
    GemQuartzProperties gemQuartzProperties;

    @Override
    public boolean startJob(JobsVo jobsVo) {
        boolean result = quartzManager.startJob(jobsVo);
        //执行成功，修改状态
        if(result){
            Jobs jobs = new Jobs();
            jobs.setId(jobsVo.getId());
            jobs.setStatus(JobStatus.START.getCode());
            return updateById(jobs);
        }
        return result;
    }

    @Override
    public boolean stopJob(JobsVo jobsVo) {
        boolean result = quartzManager.stopJob(jobsVo);
        //执行成功，修改状态
        if(result){
            Jobs jobs = new Jobs();
            jobs.setId(jobsVo.getId());
            jobs.setStatus(JobStatus.STOP.getCode());
            return updateById(jobs);
        }
        return result;
    }

    @Override
    public boolean modifyJobTime(JobsVo jobsVo) {
        Jobs entity = GemBeanUtils.copyProperties(jobsVo, Jobs.class);
        return updateById(entity);
    }

    @Override
    public boolean initJobs() {
        //定时任务初始化方式：
        // 默认1:初始化全部停止状态；
        // 2:初始化全部运行状态，并启动任务
        // 3:保持原状态，并启动任务
        if(gemQuartzProperties.getJobInit()== JobInit.ALL_STOP_STATUS.getCode()){
            jobsMapper.updateAllStatus(JobStatus.STOP.getCode());
        }else if(gemQuartzProperties.getJobInit()== JobInit.ALL_START_STATUS.getCode()){
            List<Jobs> list = this.list();
            if(list!=null && list.size()>0){
                for(Jobs jobs:list){
                    JobsVo jobsVo = GemBeanUtils.copyProperties(jobs,JobsVo.class);
                    quartzManager.startJob(jobsVo);
                }
            }
            jobsMapper.updateAllStatus(JobStatus.START.getCode());
        }else if(gemQuartzProperties.getJobInit()== JobInit.KEEP_START_STATUS.getCode()){
            List<Jobs> list = this.list();
            if(list!=null && list.size()>0){
                for(Jobs jobs:list){
                    JobsVo jobsVo = GemBeanUtils.copyProperties(jobs,JobsVo.class);
                    if(jobsVo.getStatus()==JobStatus.START.getCode()){
                        quartzManager.startJob(jobsVo);
                    }
                }
            }
        }else{
            jobsMapper.updateAllStatus(JobStatus.STOP.getCode());
        }
        return true;
    }

}