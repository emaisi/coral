/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.jobs;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.util.Date;

/**
 * @Title: JobTest
 * @Package: com.gemframework.modules.perkit.quartz.jobs
 * @Date: 2020-06-19 23:48:07
 * @Version: v1.0
 * @Description: Job测试类
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
public class JobDemo implements Job {


    /**
     * 重写任务内容
     * @param jobExecutionContext
     */
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        //获取参数
        JobDataMap dataMap = jobExecutionContext.getMergedJobDataMap();
        String param = null;
        if(dataMap!=null){
            if(dataMap.getString("param")!=null){
                param = dataMap.getString("param");
            }
        }
        try {
            log.info("{}执行任务:默认参数：{}",new Date(),param);
        } catch (Exception e) {
            log.info("Quartz执行失败");
        }
    }
}
