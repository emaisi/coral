/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gemframework.model.common.BaseEntityPo;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@TableName("gem_sys_jobs")
@Data
public class Jobs extends BaseEntityPo {

    //任务名称
    private String jobName;
    //任务组名称
    private String jobGroup;
    //触发器名称
    private String triggerName;
    //触发器组名称
    private String triggerGroup;

    //任务类型 Simple 和 Cron
    private Integer jobType;
    //任务参数
    private String jobParams;

    //Cron任务表达式
    private String triggerCronExpression;

    //执行开始时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date triggerStartTime;

    //执行结束时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date triggerStopTime;

    //SpringBean类名称
    private String executeClassName;
    //SpringBean方法名称
    private String executeMethodName;
    //任务表名
    private String targetTable;
    //运行状态 0=暂停，1=运行
    private Integer status;

}
