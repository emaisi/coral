***
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
***

### 客户端调用API示例代码

##### 生成签名

`	function getSignature(params) {
        var publicKey="123";
        var apiVersion = "1.0";
        var clientOs = "iOS";
        var clientType = "HTML5";
        var timestamp = Date.parse(new Date());
        var nonce = randomNum(8);
        var signMethod = "HMAC-SHA1";
        var signData = {"Api-Version":apiVersion,"Client-OS":clientOs,"Client-Type":clientType,"TimeStamp":timestamp,"Nonce":nonce,"Sign-Method":signMethod};
        var signParams=jsonSort(signData);
        var encodeSignParams = encodeURIComponent(signParams+"&"+params);
        //使用算法 第一个参数为加密字符串，第二个参数为公共秘钥
        var sign=CryptoJS.HmacSHA1(encodeSignParams,publicKey+"&");
        var base64Sign = window.btoa(sign);
        signData.Sign = base64Sign;
        return signData
	}`

##### 调用示例

`function(){
	var url="http://127.0.0.1:82/api/getAccessToken";
	var data = {"account":"ceshi1", "password":"123"};
	var params = jsonSort(data);
    var signData = getSignature(params);
    $.ajax({
		type: "GET",
		url: url,
		headers:signData,
		data:data,
		dataType:"json",
		contentType: "application/x-www-form-urlencoded",
		success: function(res){
			$("#div1").html("请求结果："+JSON.stringify(res));
			accessToken = res.data.accessToken;
			refreshToken = res.data.refreshToken;
		}
	});
}`