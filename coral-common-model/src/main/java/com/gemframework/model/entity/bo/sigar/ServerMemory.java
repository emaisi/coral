/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Title: ServerMemory
 * @Package: com.gemframework.model.entity.bo.sigar
 * @Date: 2020-07-30 22:48:42
 * @Version: v1.0
 * @Description: 服务器内存信息
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
@Builder
public class ServerMemory implements Serializable {

	private static final long serialVersionUID = 1L;

	//内存总量:300G
	private double total;
	//当前内存使用量:    200G used
	private double used;
	//当前内存剩余量:    100G free
	private double free;
	//交换区总量:    30G av
	private double swapTotal;
	//当前交换区使用量:   20G used
	private double swapUsed;
	//当前交换区剩余量:   10G free
	private double swapFree;
}
