/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Title: ServerWhoInfo
 * @Package: com.gemframework.model.entity.bo.sigar
 * @Date: 2020-07-30 23:19:41
 * @Version: v1.0
 * @Description: 服务器用户
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
@Builder
public class ServerWhoInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	//用户索引
	private Integer id;
	//用户控制台:    console
	private String console;
	//用户host:    NT AUTHORITY
	private String host;
	//当前系统进程表中的用户名:    LOCAL SERVICE
	private String userName;
}
