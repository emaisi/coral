/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Title: ServerProperty
 * @Package: com.gemframework.model.entity.bo.sigar
 * @Date: 2020-07-30 23:20:07
 * @Version: v1.0
 * @Description: 服务器属性
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
@Builder
public class ServerProperty implements Serializable {

	private static final long serialVersionUID = 1L;

	//用户名:    fly
	private String userName;
	//用户的主目录：    C:\Users\fly
	private String userHome;
	//用户的当前工作目录：    F:\Project\LovePandaJ\trunk\app\LovePandaJ
	private String userDir;
	//计算机域名:    LOVEPANDA
	private String userDomain;
	//计算机名:    LOVEPANDA
	private String computerName;
	//本地ip地址:    192.168.0.150
	private String ipAddr;
	//本地主机名:    LovePanda
	private String hostName;
	//JVM可以使用的总内存:    127467520
	private Long jvmTotalMemory;
	//JVM可以使用的剩余内存:    126801664
	private Long jvmFreeMemory;
	//JVM可以使用的处理器个数:    4
	private Integer jvmAvailableProcessors;
	//Java的运行环境版本：    1.6.0_43
	private String javaVersion;
	//Java的运行环境供应商：    Sun Microsystems Inc.
	private String javaVendor;
	//Java供应商的URL：    http://java.sun.com/
	private String javaVendorURL;
	//Java的安装路径：    C:\Program Files\Java\jdk1.6.0_43\jre
	private String javaHome;
	//Java的虚拟机规范版本：    1.0
	private String jvmSpecificationVersion;
	//Java的虚拟机规范供应商：    Sun Microsystems Inc.
	private String jvmSpecificationVendor;
	//Java的虚拟机规范名称：    Java Virtual Machine Specification
	private String jvmSpecificationName;
	//Java的虚拟机实现版本：    20.14-b01
	private String jvmVersion;
	//Java的虚拟机实现供应商：    Sun Microsystems Inc.
	private String jvmVendor;
	//Java的虚拟机实现名称：    Java HotSpot(TM) 64-Bit Server VM
	private String jvmName;
	//Java运行时环境规范版本：    1.6
	private String javaSpecificationVersion;
	//Java运行时环境规范供应商：    null
	private String javaSpecificationVendor;
	//Java运行时环境规范名称：    Java Platform API Specification
	private String javaSpecificationName;
	//Java的类格式版本号：    50.0
	private String javaClassVersion;
	//Java的类路径：    F:\Project\LovePandaJ\trunk\app\LovePandaJ\WebRoot\WEB-INF\classes;
	private String javaClassPath;
	//加载库时搜索的路径列表：    C:\Program Files\Java\jdk1.6.0_43\bin;
	private String javaLibraryPath;
	//默认的临时文件路径：    C:\Users\fly\AppData\Local\Temp\
	private String javaTmpdirPath;
	//一个或多个扩展目录的路径：    C:\Program Files\Java\jdk1.6.0_43\jre\lib\ext;C:\Windows\Sun\Java\lib\ext
	private String javaExtdirsPath;
	//操作系统的名称：    Windows 8
	private String osName;
	//操作系统的构架：    amd64
	private String osArch;
	//操作系统的版本：    6.2
	private String osVersion;
	//文件分隔符：    \
	private String fileSeparator;
	//路径分隔符：    ;
	private String pathSeparator;
	//行分隔符：
	private String lineSeparator;

}
