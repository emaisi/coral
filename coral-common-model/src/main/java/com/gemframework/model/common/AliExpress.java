/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Title: AliExpress.java
 * @Package: com.gemframework.model.vo
 * @Date: 2019/11/30 22:40
 * @Version: v1.0
 * @Description: 阿里的快递接口返回
 * @Author: zhangysh
 * @Copyright: Copyright (c) 2019 GemStudio
 * @Company: www.gemframework.com
 */

@Data
@Builder
@AllArgsConstructor
public class AliExpress implements Serializable {

    private static final long serialVersionUID = 1;

    //number": "780098068058",
    private String number;
    //"type": "zto",
    private String type;
    ///* 0：快递收件(揽件)1.在途中 2.正在派件 3.已签收 4.派送失败 5.疑难件 6.退件签收  */
    //"deliverystatus": "3",
    private String deliverystatus;
    ///*  1.是否签收                  */
    //"issign": "1",
    private String issign;
    ///*  快递公司名称                */
    //"expName": "中通快递",
    private String expName;
    ///*  快递公司官网                */
    //"expSite": "www.zto.com",
    private String expSite;
    ///*  快递公司电话                */
    //"expPhone": "95311",
    private String expPhone;
    ///*  快递员 或 快递站(没有则为空)*/
    //"courier": "容晓光",
    private String courier;
    ///*  快递员电话 (没有则为空)     */
    //"courierPhone":"13081105270",
    private String courierPhone;
    ///*  快递轨迹信息最新时间        */
    //"updateTime":"2019-08-27 13:56:19",
    private String updateTime;
    ///*  发货到收货消耗时长 (截止最新轨迹)  */
    //"takeTime":"2天20小时14分",
    private String takeTime;
    //"logo":"
    private String logo;

    private List<Node> list;


    @Data
    public static class Node implements Serializable {
        private static final long serialVersionUID = 1;

        private String time;
        private String status;
    }
}
