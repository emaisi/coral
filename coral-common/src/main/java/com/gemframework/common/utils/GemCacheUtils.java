/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.gemframework.common.constant.GemConstant.Auth.SESSIONID_NAME;

/**
 * @Title: GemCacheUtils
 * @Package: com.gemframework.common.utils
 * @Date: 2020-07-04 11:22:32
 * @Version: v1.0
 * @Description: 缓存工具类
 * 主要定义一些缓存操作的公共方法，缓存动态Key等
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
public class GemCacheUtils {

    //根据会员ID获取AccessToken的key
    public static String getAccessTokenKeyByMemberId(Long memberId){
        return  "accessToken_memberId:"+memberId;
    }

    //根据会员ID获取RefreshToken的key
    public static String getRefreshTokenKeyByMemberId(Long memberId){
        return  "refreshToken_memberId:"+memberId;
    }

    //根据token获取AccessToken的key
    public static String getTokenKey(String token){
        return "accessToken:"+token;
    }

    //根据sessionId获取租户Key
    public static String getTenantKey(String sessionId){
        return "tenant:"+sessionId;
    }

    //通过cookies获取sessionId
    private String getSessionId(HttpServletRequest request){
        String sessionId = "";
        for (Cookie c:request.getCookies()){
            if(c.getName().equals(SESSIONID_NAME)){
                sessionId = c.getValue();
            }
        }
        log.info("sessionId="+sessionId);
        return sessionId;
    }
}


