package com.gemframework.common.config.sigar;

import com.gemframework.common.utils.GemConfigUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hyperic.jni.ArchNotSupportedException;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarLoader;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.*;

@Slf4j
@Configuration
public class GemSigarConfig {

    //静态代码块
    static {
        try {
            String sigarPath = (String) GemConfigUtils.getProperties("gem.sigar.path");
            if(sigarPath == null || StringUtils.isBlank(sigarPath)){
                sigarPath = (String) GemConfigUtils.getYmlConfig("gem.sigar.path");
            }
            initSigar(sigarPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //初始化sigar的配置文件
    public static void initSigar(String sigarPath) throws IOException {
        SigarLoader loader = new SigarLoader(Sigar.class);
        String lib = null;

        try {
            lib = loader.getLibraryName();
            log.info("init sigar os/dll:"+lib);
        } catch (ArchNotSupportedException var7) {
            log.error("initSigar() error:{}",var7.getMessage());
        }
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("classpath:/sigar.so/" + lib);
        if (resource.exists()) {
            InputStream is = resource.getInputStream();
            File tempDir = new File(sigarPath);
            if (!tempDir.exists()){
                tempDir.mkdirs();
            }
            BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(new File(tempDir, lib), false));
            int lentgh = 0;
            while ((lentgh = is.read()) != -1){
                os.write(lentgh);
            }
            is.close();
            os.close();
            //设置sigar依赖包路径
            System.setProperty("org.hyperic.sigar.path", tempDir.getCanonicalPath());
        }
    }
}
